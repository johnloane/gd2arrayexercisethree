import java.util.Arrays;

/* Write a method called reverse that takes an int array as a parameter
    The method should not return a value but it is allowed to alter the array passed to it
    In main test the reverse function
    if given {1,2,3} -> {3,2,1}
 */
public class Main {
    public static void main(String[] args) {
        int[] numbersToReverse = {1,5,3,7,11,9,15};

        System.out.println(Arrays.toString(numbersToReverse));
        reverse(numbersToReverse);
        System.out.println("Numbers in reverse: " + Arrays.toString(numbersToReverse));
    }

    private static void reverse(int[] arrayToReverse)
    {
        int maxIndex = arrayToReverse.length-1;
        int halfLength = arrayToReverse.length/2;
        for(int i=0; i < halfLength; ++i)
        {
            int temp = arrayToReverse[i];
            arrayToReverse[i] = arrayToReverse[maxIndex-i];
            arrayToReverse[maxIndex-i] = temp;
        }
    }
}
